Meteor Gitlab CI
================

A Meteor Gitlab CI Example:


**Master Status**

[![build status](https://gitlab.com/memosa/meteor-gitlab-ci/badges/master/build.svg)](https://gitlab.com/memosa/meteor-gitlab-ci/commits/master) [![coverage report](https://gitlab.com/memosa/meteor-gitlab-ci/badges/master/coverage.svg)](https://gitlab.com/memosa/meteor-gitlab-ci/commits/master)

**Develop Status**

[![build status](https://gitlab.com/memosa/meteor-gitlab-ci/badges/develop/build.svg)](https://gitlab.com/memosa/meteor-gitlab-ci/commits/develop) [![coverage report](https://gitlab.com/memosa/meteor-gitlab-ci/badges/develop/coverage.svg)](https://gitlab.com/memosa/meteor-gitlab-ci/commits/develop)
