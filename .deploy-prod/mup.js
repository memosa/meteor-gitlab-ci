module.exports = {
  servers: {
    memosaDev: {
    }
  },

  meteor: {
    name: 'example',
    path: '../',
    servers: {
      memosaDev: {},
    },

    buildOptions: {
      // build with the debug mode on
      cleanAfterBuild: true, // default
      debug: true,
    },

    env: {
      ROOT_URL: 'https://example.memosa.tech',
      MONGO_URL: 'mongodb://localhost/meteor',
    },

    ssl: {
      // Enables let's encrypt (optional)
      autogenerate: {
        email: 'admin@memosa.com',
        domains: 'example.memosa.tech'
      }
    },

    // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
    docker: {
      image: 'abernix/meteord:base'
    },
    deployCheckWaitTime: 120,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: false
  },

  mongo: {
    oplog: true,
    port: 27017,
    version: '3.4.1',
    servers: {
      memosaDev: {},
    },
  },
};
